![Insertando imagen](7.jpg)
# UNIVERSIDAD CÉSAR VALLEJO

FACULTAD DE INGENIERIA
Escuela de Ingeniería de Sistemas

## Desarrollo de una aplicación web para el control del progreso de los  clientes  del  gimnasio “TOTAL FITNESS”  Lima - Lima - 2022  

TOTAL FITNESS


### Profesor RICHARD LEONARDO BERROCAL NAVARRO
ORCID: 0000-0001-8718-3150

#### EQUIPO NRO.8

|Orden|Apellidos y Nombres|Cod. Orcid|% de 1ra entregra |% de 2ra entrega| % Entrega Final|
|:|:|:|:|:|:|
|1|Ccoyori Pérez, José Antonio|(0000-0002-0082-9017)|100|100|100|
|2|Diaz Aguilar, Gian Carlos|(0000-0002-9485-6015)|90|100|100|
|3|Orosco Machacuay , Melissa Anais|(0000-0001-7756-7177)|100|100|100|
|4|Rodríguez Rojas, Kevin|(0000-0002-2811-7249)|90|0|100|
|5|Salazar Angeles, Sharon Denil|(0000-0002-8065-4789) |90|100|100|
|6|Sandi Chiquillan, Ovaldi Jesus|(0000-0002-6065-9742) |100|100|100|

RESUMEN EJECUTIVO
El presente trabajo de investigación titulado “Desarrollo de una aplicación web para el control del progreso de los  clientes  del  gimnasio “TOTAL FITNESS”  Lima - Lima - 2022  “ , nos permitirá implementar el sistema requerido para el gimnasio para que permita dar más facilidad a los trámites de la empresa en base a los estudios realizados de factibilidad, usando el lenguaje UML y RUP para entregar un producto de calidad en un tiempo prudencial.

 INTRODUCCIÓN 

A finales del 2019 , apareció una nueva enfermedad que se extendió rápidamente por todo el mundo por lo que fue declarado como pandemia , esto llevó a que los diferentes gobiernos del mundo aislen a su  población para  poder  controlar su propagación , si bien al inicio fue de mucha ayuda las personas empezaron a sufrir las consecuencias  del encierro  se presentaron situaciones que afectaron su salud física y mental en su mayoría de casos , por la tristeza , soledad , ansiedad , depresión y el miedo que generaba la incertidumbre de la situación . Esto desencadenó como consecuencia  un descuido en la alimentación de las personas  causando un alza en los casos de sobrepeso en gran cantidad de países en el mundo. Nuestro país no fue ajeno a esta situación, por lo cual los habitantes  optaron en su mayoría por  inscribirse en gimnasios con el fin de mejorar su salud, sin embargo  muchos de estos seguían utilizando control básico del progreso de los clientes por medio de registros convencionales en otras palabras un control mediante apuntes,  en el presente proyecto implementaremos una aplicación web para el control del progreso de los  clientes  del  gimnasio TOTAL FITNESS.A, que en su mayoría están en el rango de edad 18 - 50 años, el objetivo del desarrollo de la aplicación web  es brindar una información detallada del progreso (Peso, Masa Muscular), de los clientes, para que lleven un control más  ordenado de sus rutinas y así poder motivarse a seguir asistiendo al gimnasio para  seguir cuidando su salud .


1. ESTUDIO DE FACTIBILIDAD
 
 
 En el siguiente proyecto se realizará  un estudio de factibilidad donde determinaremos  si el proyecto es viable, para ello mediante criterios determinamos la factibilidad del proyecto.

|CRITERIOS|SE REQUIERE|
|:-|:-|
|Fundamentación Teórica |Acceso a la parte teórica para la fundamentación del proyecto, programas complementarios.| 
|Posibilidad de acceso a los recursos requeridos por el proyecto |Disponibilidad de recursos tecnológicos, que necesita el proyecto.| 
|Accesos a las fuentes de información.|Acceso directo a la informacion sea por web y al negocio.| 


1.1. Factibilidad operativa y técnica: La visión del sistema

FACTIBILIDAD OPERATIVA 
Existen las condiciones adecuadas para el desarrollo y su posterior comprobación. Este proyecto puede tener un buen impacto en los clientes del gimnasio TOTAL FITNESS S.A , dado que a las personas que va dirigido les gusta mantener un buen acondicionamiento físico pero los primeros meses son difíciles y tienden a desistir de entrenar, con el sistema se busca que esto no pase y puedan observar una mejora en su progreso personal y salud .

FACTIBILIDAD TÉCNICA

| Tipo | Especificaciones mínimas |Descripción|
|:|:|:|
|Procesador|Cpu 1.8 GHz| Procesador Intel Dual Core, Recomendado: Intel Core i3  o superior|
|Memoria|4 Gb| Memoria 4 Gb de RAM, DD3 con 1600 Mhz Superior, Recomendado Marca Kingston|
|Disco duro|500 Gb|Disco Dureo HDD o SSD de 500 Gb o Superior Recomendado Marca Western Digital|
|Dispositivos de entrada y salida |Estandar|Teclado Mouse estandar Marca Logitech, Monitor 24" recomendado Marca LG|
|Sistema Operativo| Windows| Sistema Operativo Microsoft Windows 7 Pro de 64 bits, procesador x64 o SUperior|

2. MODELO DEL NEGOCIO

Un diagrama de proceso empresarial es un diagrama que describe un flujo dirigido de actividades especificadas utilizando un subconjunto de notación para la creación de modelos de proceso de negocio. Un proceso simple representa los procesos internos que ocurren en una unidad organizativa o empresarial.

![Insertando imagen](1.jpg)

Imagen No 1. - Diagrama Eriksson - Penker
   

2.1. Modelo de Caso de Uso del Negocio

El cliente solicita una apertura de membresía al recepcionista del Gimnasio, el recepcionista genera formulario para apertura de membresía registra el formulario al sistema y de acuerdo a las respuestas del cliente, se toma la decisión si se apertura la membresía, si se apertura se asigna un instructor y se entregará sus credenciales,  sus que esas serán registradas y para su respectivo control.

|Lista de actores del negocio||
|:-|:-|
|Nombre|Descripción|
|Cliente|Persona que requiere el servicio, de rutina.|
|Recepcionista|Persona responsable en el registro del cliente y la asignación del entrenador, el usuario registra su avance de rutina|
|Entrenador|Persona encargada en el siguiento del cliente para el desarrollo de su rutina|

![Insertando imagen](2.jpg)

Imagen No 2. - Actores de Negocio 

2.1.2. Lista de Casos de Uso del Negocio

|Lista casos de Uso del Negocio||
|:-|:-|
|Nombre|Descripción|
|Llenar formulario|El cliente solicitará en percepción su respectivo formulario para la apertura de membresía.|
|Generar formulario de apertura.|El Recepcionista genera Formulario, para que el cliente pueda completarlos.|
|Asignar al Entrenador|Una vez que el formulario está completado, se realizará una verificación de los datos ingresados, el instructor verificará las preguntas relacionadas a su ámbito.|

Tabla No. 2 – Lista de Casos de Uso del Negocio

![Insertando imagen](3.jpg)

Imagen No.3 - Casos de uso de Negocio

2.1.3. Diagrama de Casos de Uso del Negocio

![Insertando imagen](4.jpg)

Imagen  No. 4. Diagrama de Casos de Uso del Negocio

2.1.4. Especificaciones de Casos de uso del Negocio

En la especificación de los casos de uso de negocio encontraremos paso a paso el funcionamiento de nuestro proceso.

|Nombre|Llenar formulario|
|:-|:-|
|Descripción|El cliente solicitará en percepción su respectivo formulario para la apertura de membresía.|
|Actores de negocio|Cliente, Recepcionista.|
|Entradas|Información, formularios.|
|Entregables|Membresía.|
|Mejoras |Registro, vía web para agilizar y que el cliente pueda completarlo sin dificultad.|

 Tabla No. 3.1. – ECUN  1

|Nombre|Generar formulario de apertura.|
|:-|:-|
|Descripción|El Recepcionista genera Formulario, para que el cliente pueda completarlos.|
|Actores de negocio|Recepcionista.|
|Entradas|Información del cliente.|
|Entregables|Formulario de apertura.|
|Mejoras |El formulario sea digital, y sea enviado al correo del cliente.|

Tabla No. 3.2. – ECUN  2

|Nombre|Asignar al Entrenador.|
|:-|:-|
|Descripción|Una vez que el formulario está completado, se realizará una verificación de los datos ingresados, el instructor verificará las preguntas relacionadas a su ámbito.|
|Actores de negocio|Recepcionista, entrenador.|
|Entradas|Respuestas de cliente, mediante el formulario.|
|Entregables|Registro por sistema, datos del instructor.|
|Mejoras |En el sistema web se asignará al instructor, el cliente visualizará los datos del entrenador.|

Tabla No. 3.3. – ECUN  3

|Nombre|Entregar credencial al cliente.|
|:-|:-|
|Descripción|Una vez que el formulario fue aceptado, se hace entrega de la credencial.|
|Actores de negocio|Recepcionista.|
|Entradas|Formulario aceptado.|
|Entregables|Credencial.|
|Mejoras|Con la credencial podrá registrar sus actividades, realizar un control más responsable de los ejercicios realizados.|

Tabla No. 3.4. – ECUN  4

2.2. Modelo de Análisis del Negocio

A diferencia del modelo de caso de negocio en el modelo de análisis pasamos a una vista   interna   de sus procesos, pudiendo analizar los trabajadores del  Gimnasio  “Total fitness” y además identificamos las entidades que se manejan en nuestro proceso.

2.2.1. Lista de Trabajadores de Negocio

![Insertando imagen](5.jpg)

Tabla No. 4. Trabajadores del Negocio

2.2.2. Lista de Entidades de Negocio

![Insertando imagen](6.jpg)

Tabla No. 5. Entidades del Negocio

2.2.3. Realización de Casos de Uso del Negocio

A continuación se muestran los procesos del negocio , hemos mostrado  en el diagrama de actividades las dos secuencias más importantes que son el registro del cliente  y la ficha del control de entrenamiento del instructor.

![Insertando imagen](9.jpg)

DIAGRAMA DE ACTIVIDADES :

1)PROCESOS DEL GIMNASIO TOTAL FITNESS:

![Insertando imagen](99.jpg)

2.3. Glosario de términos

![Insertando imagen](11.jpg)

2.4. Reglas de negocio

![Insertando imagen](15.jpg)


# 3. CAPTURA DE REQUERIMIENTOS

Para poder realizar estas implementación  debemos saber las funciones que tiene el gym,  y el proceso de recaudación de datos personales del  usuario,  por lo cual debemos procurar de entender todas las funciones y modalidades que pueden llegar a existir en el gym

## 3.1. Fuentes de obtención de requerimientos 

En nuestro proyecto hemos utilizado una técnica tradicional que es  el uso de  entrevistas , esta fue realizada  al administrador del gimnasio ,  para poder entender las necesidades que requiere el negocio y así  poder realizar el sistema .


### 3.1.1. Informe de entrevistas o material técnico
ENTREVISTA AL ADMINISTRADOR DEL GIMNASIO TOTAL FITNESS

La siguiente entrevista nos ayudara para poder recaudar información acerca de los procesos del negocio y los requerimientos que necesitamos para desarrollar el sistema. Toda la información es utilizada con fines académicos.
   Fecha: viernes 13/05/22
   
1) 	¿Actualmente utiliza un sistema o aplicativo para recolectar y registrar la distinta información de sus clientes?
No, pero si nos gustaría en un futuro, más adelante si formamos una aplicación o algo para que las personas estén informadas
 
2) 	¿Qué información personal de los clientes es registrada?
Registramos el nombre, su DNI, la dirección, correo electrónico y teléfono, además se clasifica en que plan de pago al que va acceder.

3) 	¿Cómo se realiza el proceso de registro de la información y datos del cliente en el gimnasio?
En el área de recepción, solo utilizamos hojas de Excel para poder registrar a los clientes, lo cual requiere mucho tiempo de procesamiento cuando queremos buscar la información específica de usuario, además de que muchas veces se cometen errores por parte de los encargados de recepción.
 
4) 	¿Lleva actualmente algún proceso evolutivo de cada cliente?
Si nosotros aquí realizamos un registro de manera manual sobre las medidas antropométricas de nuestros clientes a través de un cuaderno de apuntes de manera quincenal el encargado de realizarlo es el entrenador, para esto se registra con fecha su progreso.
 
5) 	¿Le interesaría que la distinta información de sus clientes y progreso de estos se vean reflejadas en una página web exclusiva para el gimnasio?
 Sí, nos gustaría, porque es una gran ayuda para agilizar los procesos en el área de recepción del negocio, además para que los clientes puedan visualizar el progreso en su estado físico y así estar más motivados en venir al gimnasio.


### 3.1.2. Matriz de Actividades y Requerimientos


<table>
   <tr>
	<tr>
	    <th>Proceso de Negocio</th>
	    <th>Actividad del Negocio</th>
	    <th>Responsable del negocio</th>  
       <th>Requerimiento</th> 
       <th>Casos de uso</th> 
       <th>Actores</th> 
       <th>Iteración # o Prioridad</th>  
       </tr >
	<tr >
	    <td rowspan="3">Control de Asistencia</td>
	    <td>Verificar si el cliente esta registrado</td>
	    <td rowspan="3">Encargado de recepción (Er)</td>
       <td>R01	Necesito que el sistema permita verificar el registro de clientes
</td>

<td> C01 Consultar registros de clientes.
</td>
<td rowspan="3">Encargado de recepción (Er)</td>
<td>1
</td>
	<tr>
	    <td>Registra asistencia de clientes</td>
       <td>R02	Necesito que el sistema permita registrar asistencia de clientes.
</td>
<td>C02	Registrar asistencia de clientes.
</td>
<td> 3
</td>
	</tr>
	<tr>
	    <td >Registrar cliente
   </td>
   <td>R03	Necesito que el sistema permita registrar al cliente
</td>
<td>C03	Registrar cliente
</td>
<td> 2
</td>
	</tr>
	<tr>
	    <td rowspan="6">Control de progreso</td>
	    <td>Verificar asistencia de clientes
       
   </td>
	  <td rowspan="6">Entrenador (En)</td>
     <td>R04	Necesito que el sistema permita verificar las asistencias del cliente</td>
	 <td >C04 Consultar asistencia de clientes
</td>
<td rowspan="6">Entrenador (En)</td>
<td> 4
</td>
	</tr>
   <tr>
	    <td>Verificar estado fisico del cliente
</td>
<td >R05	Necesito que el sistema permita verificar el estado fisico del cliente
</td>
<td >C05	Consultar estado de fisico del cliente.
</td>
<td> 8
</td>
	</tr>
	<tr>
	    <td >Indicar rutinas de ejercicio
</td>
<td >R06	Necesito que el sistema permita registrar rutinas de ejercicio.
</td>
<td >C06 Sugerir rutinas de ejercicio.
</td>
<td> 6
</td>
	</tr>
	<tr>
	    <td>Registrar progreso del cliente en el dia.
</td>
<td> R07	Necesito que el sistema permita registrar el progreso del cliente al dia
</td>
<td >C07 Registrar progreso del cliente en el dia.
</td>
<td> 7
</td>
	</tr>
	<tr>
	    <td>Registrar estado fisico del cliente quincenalmente
</td>
<td >R08	Necesito que el sistema permita registrar el estado fisico del cliente quincenalmente.
</td>
<td >C08 Registrar estado fisico del cliente quincenalmente
</td>
<td> 5
</td>
	</tr>
   </td>
	    <td>Entrega de resultados del progreso al cliente.
</td>
<td> R09	Necesito que el sistema permita actualizar el progreso del cliente.
</td>
<td >C09	Actualizacion del progreso de cliente.
</td>
<td>9
</td>
</table>

MATRIZ ACT. VS REQ. 

## 3.2. Modelo de Casos de Uso
En relación a la entrevista al  administrador del gimnasio  “TOTAL FITNESS”, se determinó, que el sistema permite verificar el registro de clientes, además registrar asistencia de clientes, para su verificación por parte del entrenador e iniciar sus clases. En relación a resultados, se pueda verificar el estado físico del cliente además que permita registrar rutinas de ejercicio para los clientes, todo eso se registrará en el progreso del cliente diario , juntando la información para mostrar un informe quincenal y permita actualizar el progreso del cliente para que visualice su rendimiento y logros.



### 3.2.1. Lista de Actores del Sistema

|Lista de actores del sistema||
|:-|:-|
|Nombre|Descripción|
|Recepcionista|Se encarga del registro del ingreso del cliente.|
|Entrenador|Da seguimiento al progreso del usuario.|

Tabla No. 8. Lista de Actores del Sistema

### 3.2.2. Lista de Casos de Uso del Sistema

|Lista de caso de uso de sistema| |
|:-|:-|
|Nombre|Descripción|
|Consultar registros de clientes|El área de recepción se encarga de consultar el registro de clientes.|
|Registrar cliente|El área de recepción se encarga de registrar al cliente.|
|Registrar asistencia de clientes|El área de recepción se encarga de llevar el control de la asistencia de clientes.|
|Consultar asistencia de cliente|El entrenador se encarga de consultar en el sistema si el cliente ha asistido .|
|Registrar estado físico del cliente quincenalmente |El entrenador realizará un registro del estado físico del cliente de manera quincenal.|
|Consultar rutinas de ejercicio |El entrenador sugiere una lista de rutinas de ejercicios de acuerdo a la necesidad de cada cliente .|
|Registrar el progreso del cliente en el día .|El entrenador registra el progreso del cliente después de terminar su sesión de entrenamiento diaria .|
|Consultar estado físico del cliente |El entrenador realizará un registro del estado físico del cliente de manera quincenal.|
|Actualización del progreso de cliente|El entrenador actualiza el progreso del cliente .|


Tabla No. 9. Lista de Casos de Uso del Sistema

### 3.2.3. Lista de Casos de Uso priorizados

|Priorización  Casos de Uso del sistema|||||||
|:-|:-|:-|:-|:-|:-|:-|
|||0.4|0.3|0.2|0.1|
|Actor|Caso de uso|Complejidad|Precedencia|Premura|Riesgo|Total|
|Recepcionista|Consultar registros de clientes|0.4|0.3|0.2|0.1|1|
|Recepcionista|Registrar cliente|0.3|0.2|0.2|0.0|0.7|
|Recepcionista|Registrar asistencia de clientes|0.4|0.3|0.2|0.1|1|
|Entrenador|Consultar asistencia de cliente|0.3|0.2|0.2|0.0|0.7|
|Entrenador|Registrar estado físico del cliente quincenalmente |0.3|0.3|0.2|0.1|0.9|
|Entrenador|Sugerir rutinas de ejercicio|0.2|0.1|0.1|0.1|0.5|
|Entrenador|Registrar el progreso del cliente en el día .|0.3|0.3|0.2|0.1|0.9|
|Entrenador|Consultar estado físico del cliente|0.3|0.3|0.2|0.1|0.9|
|Entrenador|Actualización del progreso de cliente|0.4|0.3|0.2|0.1|1|


Tabla No. 10. Priorización de Casos de Uso del Sistema

### 3.2.4. Diagramas de Caso de Uso del Sistema
![Insertando imagen](21.jpg)

3.2.5. Especificaciones de Requerimientos de Software

|Verificar el registro de clientes||
|:-|:-|
|Código: R01|Proceso:Control de asistencia|
|Actor:|Recepcionista|
|Prioridad en negocio: 1|Descripción: El encargado de recepción verifica si el cliente ya está registrado en el sistema.|

|Registrar asistencia de clientes||
|:-|:-|
|Código: R02|Proceso:Control de asistencia|
|Actor: |Recepcionista|
|Prioridad en negocio: 2|Descripción: El encargado de recepción registra al cliente.|

|Registra la asistencia del cliente||
|:-|:-|
|Código: R03|Proceso:Control de asistencia|
|Actor:|Recepcionista|
|Prioridad en negocio: 3|Descripción: El recepcionista después de verificar que el cliente ya está registrado procede a registrar la asistencia.|

|Verificar asistencia de clientes||
|:-|:-|
|Código: R04|Proceso:Control de progreso|
|Actor: |Entrenador|
|Prioridad en negocio: 4|Descripción: El entrenador verifica las asistencia del día de los clientes.|

|Verificar el estado físico del cliente||
|:-|:-|
|Código: R05|Proceso:Control de progreso|
|Actor:|Entrenador|
|Prioridad en negocio: 5|Descripción: El entrenador verifica en qué estado físico se encuentra el cliente.|

|Sugerir rutinas de ejercicio||
|:-|:-|
|Código: R06|Proceso:Control de progreso|
|Actor:|Entrenador|
|Prioridad en negocio: 6|Descripción: El entrenador  sugiere la rutina de ejercicio según la necesidad del cliente.|

|Registrar el progreso del cliente al dia||
|:-|:-|
|Código: R07|Proceso:Control de progreso|
|Actor:|Entrenador|
|Prioridad en negocio: 7|Descripción: El entrenador registra el progreso de sus rutinas y estado  del cliente en el día.|

|Verificar estado fisico del cliente||
|:-|:-|
|Código: R08|Proceso:Control de progreso|
|Actor:|Entrenador|
|Prioridad en negocio: 8|Descripción: El entrenador al momento que ingresa un cliente procede a ver su condición física.|

|Actualizar el progreso del cliente||
|:-|:-|
|Código: R09|Proceso:Control de progreso|
|Actor:|Entrenador|
|Prioridad en negocio: 9|Descripción: El entrenador actualiza el progreso del cliente.|

### 3.2.6. Especificaciones de Casos de Uso

|Nombre|Consultar registros de clientes|
|:-|:-|
|Tipo |Primario|
|Autor|Propuesta por el Administrador|
|Actores|Encargado de recepción|
|Iteración|1|
|Casos de uso relacionados.|Registrar asistencia de clientes (extend).|
|Breve Descripción|El caso de uso permite consultar registros de clientes del gimnasio.|
|Referencias|verificar el registro de clientes.|
|Pre condiciones|El cliente debe estar registrado en el gimnasio|
|Post condiciones|Se verifica la asistencia o inasistencia de los usuarios|

|Nombre|Registrar asistencia de clientes|
|:-|:-|
|Tipo|(Primario|
|Autor|Propuesta por el Administrador|
|Actores|Encargado de recepción|
|Iteración|3|
|Casos de uso relacionados.|Registrar cliente (include).|
|Breve Descripción|El caso de uso permite registrar las asistencias de los clientes de gimnasio.|
|Referencias|Registrar asistencia, verificar registro de clientes|
|Pre condiciones|El cliente debe estar matriculado en el gimnasio, el encargado de recepción debe haber identificado a los clientes en el registro de asistencia.|
|Post condiciones|Llenado de registro asistencia de clientes.|


|Nombre|Registrar cliente|
|:-|:-|
|Tipo |(Primario|
|Autor|Propuesta por el Administrador|
|Actores|Recepcionista|
|Iteración|2|
|Casos de uso relacionados.|Consultar registros de clientes, Registrar asistencia de clientes (include)|
|Breve Descripción|El caso de uso permite que el encargado de recepción registre al cliente.|
|Referencias|Registrar cliente, verificar registro de clientes|
|Pre condiciones|Verificar si el cliente está registrado|
|Post condiciones|Registrar cliente|

|Nombre|Consultar asistencia de clientes|
|:-|:-|
|Tipo|Secundario|
|Autor|Propuesta por el Administrador|
|Actores|Entrenador|
|Iteración|4|
|Casos de uso relacionados.|Registrar asistencia de clientes (extend).|
|Breve Descripción|El caso de uso permite que el entrenador consulte a los clientes que están presentes.|
|Referencias|Verificar las asistencias de los clientes.|
|Pre condiciones|El cliente debe haberse registrado, el recepcionista debe haber registrado la asistencia del cliente.|
|Post condiciones|El entrenador verifica la lista de clientes que asistieron.|

|Nombre|Consultar estado de fisico del cliente|
|:-|:-|
|Tipo |Secundario|
|Autor|Propuesta por el Administrador|
|Actores|Entrenador|
|Iteración|4|
|Casos de uso relacionados.|Actualización del progreso del cliente (include).|
|Breve Descripción|El caso de uso permite que el entrenador pueda verificar el estado físico del cliente.|
|Referencias|Verificar el estado físico del cliente.|
|Pre condiciones|El cliente debe haberse registrado, el recepcionista debe haber registrado la asistencia del cliente, el entrenador debe verificar las asistencias de los clientes.|
|Post condiciones |El entrenador verifica el estado físico del cliente.|

|Nombre|Sugerir rutinas de ejercicio|
|:-|:-|
|Tipo |Opcional|
|Autor|Propuesta por el Administrador
|Actores|Entrenador|
|Iteración|6|
|Casos de uso relacionados.|Registrar el progreso del día del cliente (include), registrar el estado físico del cliente quincenalmente (include).|
|Breve Descripción|El caso de uso permite que el entrenador sugiere una rutina de ejercicio al cliente.|
|Referencias|Registrar rutinas de ejercicio.|
|Pre condiciones|El cliente debe haberce registrado. El recepcionista debe haber registrado la asistencia del cliente. El entreenador debe verificar las asistencias de los clientes.El entrenador debe verificar el estado fisico de los clientes.|
|Post condiciones |El sugiere una rutina de ejercicios al cliente.|

|Nombre|Registrar el progreso del cliente en el día.|
|:-|:-|
|Tipo |(Secundario|
|Autor|Propuesta por el Administrador
|Actores|Entrenador|
|Iteración|7|
|Casos de uso relacionados.|Sugerir rutina de entrenamiento (include), registrar el estado físico del cliente quincenalmente (include).|
|Breve Descripción|El caso de uso permite que el entrenador registre el progreso del cliente durante la sesión del día.|
|Referencias|Registrar el progreso del día del cliente.|
|Pre condiciones|El cliente debe haberse registrado. El recepcionista debe haber registrado la asistencia del cliente. El entrenador debe verificar las asistencias de los clientes El entrenador debe verificar el estado físico de los clientes. El entrenador debe haber sugerido una rutina de ejercicios al cliente.|
|Post condiciones |El entrenador registra el progreso del cliente en la sesion del dia.|

|Nombre|Registrar estado físico del cliente quincenalmente|
|:-|:-|
|Tipo |Secundario|
|Autor|Propuesta por el Administrador
|Actores|Entrenador|
|Iteración|5|
|Casos de uso relacionados.|Sugerir rutina de entrenamiento (include), registrar el progreso del cliente en el día (include)|
|Breve Descripción|El caso de uso permite que el entrenador registre el cambio del estado físico del cliente durante la quincena que asistió.|
|Referencias|Registra el estado físico del cliente quincenalmente|
|Pre condiciones|El cliente debe haberse registrado. El recepcionista debe haber registrado la asistencia del cliente. El entrenador debe verificar las asistencias de los clientes.El entrenador debe verificar el estado físico de los clientes. El entrenador debe haber sugerido una rutina de ejercicios al cliente. El entrenador registra el progreso del cliente durante la sesión del día.|
|Post condiciones|El entrenador registra los cambios físicos del cliente durante la quincena.|

|Nombre|Actualización del progreso de cliente|
|:-|:-|
|Tipo|Secundario|
|Autor|Propuesta por el Administrador|
|Actores|Entrenador|
|Iteración|9|
|Casos de uso relacionados.|Consulta de estado físico del cliente (include).|
|Breve Descripción|El caso de uso permite que el entrenador actualice el estado físico del cliente, permitiendo tener una base de datos con la cual hacer comparaciones y ver el progreso del cliente.|
|Referencias|Actualización del progreso del cliente.|
|Pre condiciones|El cliente debe estar registrado en el gimnasio|
|Post condiciones|El entrenador actualiza los datos del cliente para su comparación y visualización.|


CONCLUSIONES

En el proyecto mencionado lo que hemos podido aprender en el transcurso de las 16 sesiones de clases, es poder desarrollar mediante el lenguaje UML y RUP, una aplicación web del gimnasio “TOTAL FITNESS”, para poder ayudar con aquellos sectores que carecían de eficacia en la empresa como el registros y asistencias de clientes, además de esto agregando herramientas como un registro de progresos y rutinas. 

Este proyecto nos deja en claro la importancia de UML para la elaboración, visualización y documentación de proyectos de diseño de software, como en este caso fue un apoyo para la correcta y ordenada construcción de nuestro aplicativo web para el gimnasio “TOTAL FITNESS” siguiendo paso a paso los procedimientos brindados, usando las diversas herramientas para concluir con nuestro proyecto de manera exitosa.

### Anexos

![Insertando imagen](anexo1.png)
Anexo 1.- Ingreso al sitio web

![Insertando imagen](anexo2.png)
Anexo 2.- Registrar cliente

![Insertando imagen](anexo3.png)
Anexo 3.- Consultar registro del cliente.

![Insertando imagen](anexo4.png)
Anexo 4.- Consultar rutina de ejercicio